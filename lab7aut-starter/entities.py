import pygame
import random
from helpers import Geometry

class Player(object):
    def __init__(self, screen, image):
        self.screen = screen
        self.sw , self.sh = screen.get_size()
        self.w, self.h = [100, 70]
        self.x, self.y = self.sw/2, self.sh/2
        self.angle = 0
        self.vel = 2
        self.dir = Geometry.get_direction_from_angle(self.angle, self.vel)
        self.image = image
        self.drawn = None
    def update(self):
        self.handle_events()
        if self.w > 70:
            self.w -= 0.5
        if self.h >50:
            self.h -= 0.5
        self.dir = Geometry.get_direction_from_angle(self.angle, self.vel)    

        self.move()    
    def draw(self):
        ship_img = pygame.transform.scale(self.image,(self.w, self.h))
        ship_img = pygame.transform.rotate(ship_img, -self.angle)
        self.rect = ship_img.get_rect(center=(self.x, self.y))
        self.drawn = self.screen.blit(ship_img, self.rect)
    def handle_events(self):
        mousepos = pygame.mouse.get_pos()
        self.angle = Geometry.get_angle_from_line(self.x, self.y, mousepos[0], mousepos[1]) 


    def move(self):
        self.x = self.x + self.dir[0]
        self.y = self.y + self.dir[1]    

class Asteroid(object):
    def __init__(self, screen):
        self.screen = screen
        self.sw, self.sh = screen.get_size()
        self.radius = random.randint(20,60)
        self.init_random_position()
        self.dx, self.dy = (1, 1)
        self.vel = random.choice([0, 1, 2])
    def update(self):
        self.move()
    def draw(self):
        pygame.draw.circle(self.screen, (33,33,33), (self.x,self.y), self.radius)

    def init_random_position(self):
        self.x = random.randint(0+self.radius, self.sw-self.radius)
        self.y = random.randint(0+self.radius, self.sh-self.radius)
    def move(self):
        if self.x <= self.radius or self.x >= self.sw - self.radius:
            self.dx *= -1
        if self.y <= self.radius or self.y >= self.sh - self.radius:
            self.dy *= -1

        self.x = self.x +self.dx*self.vel
        self.y = self.y +self.dy*self.vel

    