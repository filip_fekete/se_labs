# ukljucivanje biblioteke pygame
import pygame

pygame.init()
pygame.font.init()
# definiranje konstanti za velicinu prozora
WIDTH = 1024    
HEIGHT = 600
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
time = 0
# tuple velicine prozora
size = (WIDTH, HEIGHT)

myfont = pygame.font.SysFont('Arial', 40)
name = myfont.render("Gale cigan", True, GREEN)

#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Nasa kul igra")
center = [WIDTH/2 - name.get_width()/2, HEIGHT/2 - name.get_height()/2 ]
name_pos = center 
clock = pygame.time.Clock()
bgimg =pygame.image.load("trippy.jpg")
icon = pygame.image.load("icon.png")
icon = pygame.transform.scale(icon, (100, 100))
vel = 3
dx = 1*vel
dy = 1*vel
iconx = 0
icony = 0
space_down = False 
bgimg = pygame.transform.scale(bgimg, (WIDTH, HEIGHT))
bg = BLACK
done = False
while not done:
    
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                done = True
                
            if event.key == pygame.K_SPACE:
                if bg == RED:
                    bg = BLUE
                elif bg == BLUE:
                    bg = BLACK     
                elif  bg == BLACK:
                    bg = RED

            if event.key == pygame.K_1:
                vel = 1
            if event.key == pygame.K_2:
                vel = 2
            if event.key == pygame.K_3:
                vel = 3    
            if event.key == pygame.K_SPACE:    
                space_down = True
                    
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_SPACE:
                space_down = False     



    #racunjanje
    if time < 2000:
        vel = 5 * (time/1000)
    else:
        vel = 10    
    if name_pos[0] >= WIDTH - name.get_width():
        dx = -1
    if name_pos[1] >= HEIGHT - name.get_height():
        dy = -1  
    if name_pos[0] <= 0:
        dx = 1  
    if name_pos[1] <= 0:
        dy = 1      
    name_pos[0] = name_pos[0] + dx*vel
    name_pos[1] = name_pos[1] + dy    * vel  
    if space_down:
        bg = BLUE 
    else:
        bg = GREEN    
                 
                      
    screen.fill(bg)
    screen.blit(bgimg, (0,0))
    screen.blit(icon, (iconx, icony))
    screen.blit(name, name_pos )          
       

    pygame.display.flip()

    #ukoliko je potrebno ceka do iscrtavanja 
    #iduceg framea kako bi imao 60fpsa
    dt = clock.tick(60)
    time += dt
    

pygame.quit()
