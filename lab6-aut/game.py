class Icon(object):
    def __init__(self, screen, figure, width, height):
        self.screen = screen
        self.figure =figure
        self.sh = height
        self.sw = width

        self.x = 100
        self.y = 100
        self.w = 100
        self.h = 100
        self.dx = 1
        self.dy = 1
        self.vel = 1
        self.alive = True
        self.drawn = None

    def update(self):
        self.handle_events()
        if not self.alive:
            return None
        if self.x > (self.sw-self.w) or self.x < 0:
            self.dx *= -1
            self.shrink()
            self.speed_up()
        if self.y > (self.sh-self.h) or self.y < 0:
            self.dy *= -1  
            self.shrink()
            self.speed_up()   
        self.x += (self.vel * self.dx)
        self.y += (self.vel * self.dy)

    def draw(self):
        if not self.alive:
            return None
        scaled = pygame.transform.scale(self.figure, (self.w, self.h))
        
        self.drawn = self.screen.blit(scaled, [self.x, self.y])
        return self.drawn   
    def move_to_random_location(self):
        self.x = random.randint(0, self.sw-self.w)
        self.y = random.randint(0, self.sh- self.h)  
    def shrink(self):
        if self.w > 30:
            self.w -= 1
        if self.h > 30:
            self.h -= 1    
        
    def speed_up(self):
        if self.vel < 20:
            self.vel += 1
    def handle_events(self):
        pressed = pygame.mouse.get_pressed() 
        if pressed[0]:
            pos = pygame.mouse.get_pos()
            if self.drawn.collidepoint(pos):
                self.alive = False

            


# ukljucivanje biblioteke pygame
import pygame


pygame.init()
# definiranje konstanti za velicinu prozora
WIDTH = 1024
HEIGHT = 600
import random
WHITE = (255,255,255)
# tuple velicine prozora
size = (WIDTH, HEIGHT)
myfont = pygame.font.SysFont('Arial',40)
text = myfont.render("Welcome", True, WHITE)
text1 = myfont.render("Game Over", True, WHITE)
#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Nasa kul igra")

#y = random.randint(0, 600), 
#x = random.randint(0, 1024), 

clock = pygame.time.Clock()
center = [WIDTH/2 - text.get_width()/2 , HEIGHT/2 - text.get_height()/2 ]
bgimg = pygame.image.load("bg.jpg")
bgimg = pygame.transform.scale( bgimg, (WIDTH, HEIGHT))
icon = pygame.image.load("icon.png")
icon_pos = [WIDTH/2 - icon.get_width()/2 , HEIGHT/2 - icon.get_height()/2 ]
dx = -1
dy = 1
time = 0
done = False
state = "welcome" # welcome, game, gameover
icon_obj = Icon(screen, icon, WIDTH, HEIGHT)
icon_obj.move_to_random_location()


icons = []
for i in range(8):
    x = (Icon(screen, icon, WIDTH, HEIGHT))
    x.move_to_random_location()
    icons.append(x)



while not done:
    hit=False
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
         
        if event.type == pygame.KEYDOWN:
           
            if event.key == pygame.K_RETURN:
                if state == "welcome":
                    state = "game"
                if state == "gameover":
                    time = 0
                    state = "welcome"
            if event.key == pygame.K_ESCAPE:
                if state == "welcome":
                    done = True
                elif state == "game":
                    state = "gameover"
                elif state == "gameover":
                    done = True
            

    if state == "welcome":
        if time > 3000:
            state = "game"
    elif state == "game":
        icon_obj.update()
        for i in icons:
            i.update()

    if state == "welcome":
        screen.blit(bgimg, (0, 0))
        screen.blit(text, center)
    elif state == "game":
        screen.blit(bgimg, (0, 0))
        icon_drawn= icon_obj.draw()
        for i in icons:
            i.draw()
    elif state == "gameover":
        screen.blit(bgimg, (0, 0))
        screen.blit(text1, center)
    #ukoliko je potrebno ceka do iscrtavanja 
    #iduceg framea kako bi imao 60fpsa
    dt = clock.tick(60)
    time += dt
    pygame.display.flip()

pygame.quit()
