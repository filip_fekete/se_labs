# ukljucivanje biblioteke pygame
import pygame
import random
pygame.init()
pygame.font.init()
# definiranje konstanti za velicinu prozora
WIDTH = 1024
HEIGHT = 600
BLUE = (0, 0, 255)
RED = (255, 0, 0)
# tuple velicine prozora
size = (WIDTH, HEIGHT)

myfont = pygame.font.SysFont('Arial', 40)
welcome_text = myfont.render("Welcome", True, BLUE)
gameover_text =myfont.render("Game over", True, RED)
#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Nasa kul igra")


clock = pygame.time.Clock()
bgimg =pygame.image.load("isus.jpg")
icon = pygame.image.load("vrag.png")
icon = pygame.transform.scale(icon, (100, 100))
done = False
bgimg = pygame.transform.scale(bgimg, (WIDTH, HEIGHT))

center = [WIDTH/2 - icon.get_width()/2, HEIGHT/2 - icon.get_height()/2 ]
icon_pos = center

vel = 1
dx = 1
dy = 1

state = "welcome" #welcome, game, gameover


time = 0




while not done:
    hit = False
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.MOUSEBUTTONDOWN:
            click_pos = event.pos
            if icon_drawn.collidepoint(click_pos):
                hit = True
            
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                if state == "welcome":
                    state = "game"
                if state == "gameover":
                    state = "welcome"
                    time = 0 
                       
            if event.key == pygame.K_ESCAPE:
                if state == "welcome":
                    done = True
                elif state == "game": 
                    state = "gameover"
                elif state == "gameover":

                    done = True   
                       
            if event.key == pygame.K_SPACE:
                x = random.randint(0, 1024)
                y = random.randint(0, 600)
                icon_pos[0] = x
                icon_pos[1] = y


    #state handling
    if state == "welcome":
        if time > 3000:
            state = "game"
    elif state == "game":    

        if icon_pos[0] >= WIDTH - icon.get_width():
            dx = -1
        if icon_pos[1] >= HEIGHT - icon.get_height():
            dy = -1  
        if icon_pos[0] <= 0:
            dx = 1  
        if icon_pos[1] <= 0:
            dy = 1      
        icon_pos[0] = icon_pos[0] + dx*vel
        icon_pos[1] = icon_pos[1] + dy    * vel   

        if hit:
            icon_pos[0] = random.randint(0, WIDTH - 100)
            icon_pos[1] = random.randint(0, HEIGHT - 100 )            
    

    #ukoliko je potrebno ceka do iscrtavanja 
    #iduceg framea kako bi imao 60fpsa
    if state == "welcome":
        screen.blit(bgimg, (0,0))
        screen.blit(welcome_text, (WIDTH/2, HEIGHT/2))
    elif state == "game":
        screen.blit(bgimg, (0,0))
        icon_drawn = screen.blit(icon, (icon_pos[0], icon_pos[1]))
        
    elif state == "gameover":
        screen.blit(bgimg, (0,0))
        screen.blit(gameover_text, (WIDTH/2, HEIGHT/2))    
    pygame.display.flip()    
    dt = clock.tick(60)
    time += dt 
pygame.quit()
