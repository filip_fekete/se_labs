def winner(player1, player2): #funkcija winner
    if player1 == player2:
        print("draw")
    elif player1 == "rock":
        if player2 == "paper":
            return("player2")  #vraca string // return umjesto print-a
        elif player2 == "scissors":
            return("player1")
    elif player1 == "paper":
        if player2 == "rock":
            return("player1")
        elif player2 == "scissors":
            return("player2")
    elif player1 == "scissors":
        if player2 == "rock":
            return("player2")
        elif player2 == "paper":
            return("player1")

def print_winner(result):  #druga funkcija
    if result == "draw":
        print("Nerjeseno je")
    else:
        print("Pobjedio je", result)

def main():
    while True:
        player1 = input("rock-paper-scissors: ")
        player2 = input("rock-paper-scissors: ")

        result= winner(player1, player2) #poziv funkcije
        print_winner(result)

        nastavi = input("nastavi yes/no")
        if nastavi == "no":
            break

if _name_=="_main_": #uvijek cemo radit ovako(copy paste)
    main()